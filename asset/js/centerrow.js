if (!CenterRow) {
    var CenterRow = {};
}

(function($) {

    CenterRow.megaMenu = function (menuSelector, customMenuOptions) {
        if (typeof menuSelector === 'undefined') {
            menuSelector = 'header nav';
        }

        var menuOptions = {
            /* prefix for generated unique id attributes, which are required
             to indicate aria-owns, aria-controls and aria-labelledby */
            uuidPrefix: "accessible-megamenu",

            /* css class used to define the megamenu styling */
            menuClass: "nav-menu",

            /* css class for a top-level navigation item in the megamenu */
            topNavItemClass: "nav-item",

            /* css class for a megamenu panel */
            panelClass: "sub-nav",

            /* css class for a group of items within a megamenu panel */
            panelGroupClass: "sub-nav-group",

            /* css class for the hover state */
            hoverClass: "hover",

            /* css class for the focus state */
            focusClass: "focus",

            /* css class for the open state */
            openClass: "open",

            openOnMouseover: true,
        };

        $.extend(menuOptions, customMenuOptions);

        $(menuSelector).accessibleMegaMenu(menuOptions);
    };

    CenterRow.mobileMenu = function() {
        $('.menu-button').click( function(e) {
            e.preventDefault();
            $('header nav > ul').toggleClass('open');
        });
    };

    $(document).ready(function() {

        const $body = $('body');
        const $root = $('html, body');
        const $navMenu = $('nav.metadata-menu');
        const $mobileMenu = $('.collections-and-language-menu');
        const $mobileSearchMenu = $('.top-toolbar-search-options');
        const $asideSearchFilters = $('.search-facets');
        const $backToTopBtn = $('a.back-to-top');
        const $carouselSlideTitle = $('.carousel-block .slide-text .title');
        const $toolsTool = $('.tools-tool');

        /* Liens externes */

        $('a[href^="http"]').attr('target', function() {
            if (this.host === location.host) {
                return $(this).attr('target') == '_blank' ? '_blank' : '_self';
            }
            else return '_blank';
        });

        /* Header */

        $('.dropdown-menu-btn').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            $navMenu.toggleClass('open');
            $mobileMenu.removeClass('open');
            $asideSearchFilters.removeClass('open');

            if ($mobileSearchMenu.hasClass('open')) {
                let top = 0;
                if (window.innerWidth < 788) {
                    top = $('.logo-toolbar-burger-and-title').height() + $mobileSearchMenu.height() - 20;
                }
                $navMenu.css('top', top + 'px')
            }
        });

        $('#mobile-search-toggle-btn').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            $mobileSearchMenu.toggleClass('open');
            $mobileMenu.removeClass('open');
            $navMenu.removeClass('open');
            $asideSearchFilters.removeClass('open');
        });

        $('.hamburger').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $mobileMenu.toggleClass('open');
            $mobileSearchMenu.removeClass('open');
            $navMenu.removeClass('open');
            $asideSearchFilters.removeClass('open');
        })

        $mobileMenu.on('click', function(e) {
            e.stopPropagation();
        });

        $body.on('click', function(e) {
            const $target = $(e.target);

            $navMenu.removeClass('open');
            $mobileMenu.removeClass('open');
            $asideSearchFilters.removeClass('open');
            $toolsTool.removeClass('opened');

            if (! $target.closest($mobileSearchMenu).length) {
                $mobileSearchMenu.removeClass('open');
            }
        })

        $backToTopBtn.click(function($event) {
            $event.preventDefault();
            $root.animate({
                scrollTop: 0
            }, 500);
        });


        /* Page item */

        $('.viewers').prepend($('.universal-viewer'));

        $('.tabs li a').on('click', function(e) {
            e.preventDefault();

            const $allTabs = $(this).closest('.tabs').find('li');
            const $selectedTab = $(this).closest('li');

            // Tabs
            $allTabs.removeClass('active');
            $selectedTab.addClass('active');

            // Panels
            $allTabs.each(function(no, item) {
                const targetCssClass = $(item).attr('data-target');
                $('.' + targetCssClass).removeClass('active-tab-panel');
            });

            const targetCssClass = $selectedTab.attr('data-target');
            $('.' + targetCssClass).addClass('active-tab-panel');
        });

        let propertiesHeight = 0, hasHiddenProperties = false;
        const propertiesHeightMax = 500;
        $('.properties .property').each(function(no, item) {
            const $item = $(item);
            propertiesHeight += $item.height();
            if (propertiesHeight > propertiesHeightMax) {
                $item.addClass('hidden-property');
                hasHiddenProperties = true;
            }
        });

        if (hasHiddenProperties) {
            const $showAllPropertiesBtn = $('.show-all-properties-toggle')
            $showAllPropertiesBtn.addClass('needed');
            $showAllPropertiesBtn.find('a').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('opened');
                $('.hidden-property').toggleClass('show');
            });
        }

        // Boutons barre outils item.

        $('.tools .share-btn').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            const tooltip = $('.share__tooltip');
            // $(this).append(tooltip);
            tooltip.addClass('opened');
        });

        $('.tools .quote-btn').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            const citation = $('.tools-quote').text().trim();
            navigator.clipboard.writeText(citation);
            alert(citation);
            return false;
        });

        $('.tools .print-btn').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            print();
            return false;
        });

        $('.tools .download-btn').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            const link = $(this);
            const url = link.data('url');
            link.attr('href', url);
             // Create an element to force direct download
            // link.click();
            var el = document.createElement('a');
            el.setAttribute('href', url);
            el.setAttribute('download', link.attr('download'));
            el.setAttribute('style', 'display: none;');
            document.body.appendChild(el);
            el.click();
            document.body.removeChild(el);
            return true;
        });

        /*
        $(iiifCopy).on('click', function(e) {
            navigator.clipboard.writeText($(this).data('iiifUrl'));
            alert('Url du manifeste IIIF copié dans le presse-papier !');
        });
        */

        // Recherche simple
        $('.index.search .resource-link img').wrap("<div class='thumbnail'></div>");

        // Item : ressource liée
        // $('#linked-resources .resource-link img').wrap("<div class='thumbnail'></div>");

        $('.item.resource.browse ul.resource-list li.item.resource').each(function (no, item) {
            const $item = $(item);
            console.log(no, $item, $item.find('a.thumbnail'))
            if (! $item.find('a.thumbnail').length) {
                const $url = $item.find('a').attr('href');
                $item.prepend('<a class="thumbnail" href="' + $url + '"><div class="img"></div></a>')
            }
        });

        // Formulaire (ex : register) : meta avec expand button
        $('.field-meta a.expand').on('click', function(e) {
            e.preventDefault();
            $(e.target).closest('.field-meta').toggleClass('expanded');
        })

        // Filtres
        $('.search-results .search-results-list h3').wrap("<div class='items-count-and-filters-toggle'></div>");
        $('.items-count-and-filters-toggle').prepend('<a href="#" class="filters-toggle">Filtres &gt;</a>')

        $('.filters-toggle').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $asideSearchFilters.toggleClass('open');
            $mobileSearchMenu.removeClass('open');
            $navMenu.removeClass('open');
            $mobileMenu.removeClass('open');
        })

        // Slider Accueil : titre des slides du carousel, répartis en biseau sur plusieurs lignes
        $carouselSlideTitle.each(function(no, item) {

            const texte = $(item).text();
            let wmax = 23;

            const words = $.trim(texte).split(' ');

            let i, n = words.length, word, w = 0, lines = [], line = [], lineAsStarted = false;
            for(i=0;i<n;i++) {
                word = words[i];
                if (w + word.length < wmax) {
                    line.push(word);
                    w += word.length;
                } else {
                    lines.push(line.join(' '));

                    line = [word];
                    w = word.length;
                    wmax = Math.max(15, wmax - 2);
                }
            }

            if (line.length) {
                lines.push(line.join(' '));
            }

            $(item).html(lines.join('<span></span>'));
        });


        // Sliders Initialization :
        if (typeof $body.slick === "function") {

            // Vitrine de collections : effet slider pour le responsive
            $('.item-set-showcase').each(function(no, item) {
                const $slider = $(item);
                $slider.slick({
                    slide: ".item-set",
                    infinite: false,
                    speed: 300,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    arrows: true,
                    dots: true,
                    responsive: [
                        {
                            breakpoint: 1200,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                infinite: false,
                                dots: false,
                                arrows: false,
                            }
                        },
                        {
                            breakpoint: 1100,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                infinite: false,
                                dots: true,
                                arrows: true,
                            }
                        },
                        {
                            breakpoint: 900,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: true,
                                arrows: true,
                            }
                        },
                        {
                            breakpoint: 640,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: true,
                                arrows: true,
                            }
                        },
                    ]
                });
            });
            $('.item-set-showcase .caption').wrapInner( "<div class='caption-content'></div>");

            /* Block Browser preview - slider ( preview-block-slider ) */
            $('.preview-block-slider ul.resource-list').each(function(no, item) {
                const $slider = $(item);
                $slider.slick({
                    slide: ".item-set",
                    infinite: false,
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: true,
                    responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    infinite: false,
                                    dots: false,
                                    arrows: false,
                                }
                            },
                            {
                                breakpoint: 1100,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    infinite: false,
                                    dots: true,
                                    arrows: true,
                                }
                            },
                            {
                                breakpoint: 900,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true,
                                    arrows: true,
                                }
                            },
                            {
                                breakpoint: 640,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true,
                                    arrows: true,
                                }
                            },
                        ]
                });
            });
        }

    });

})(jQuery)
