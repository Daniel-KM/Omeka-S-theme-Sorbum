<?php declare(strict_types=1);

namespace OmekaTheme\Helper;

trait ThemeFunctionsSpecific
{
    /**
     * Check if a resource belongs to another site and get it.
     *
     * Dnas le cas d'un sous-site, retourne au site en cours si la ressource y
     * est présente.
     * Dans le cas du site principal, l'option permet de renvoyer au site externe,
     * même si la ressource est aussi dans le site principal.
     */
    public function externalSite(\Omeka\Api\Representation\AbstractEntityRepresentation $resource, bool $mainSiteExternal = false): ?\Omeka\Api\Representation\SiteRepresentation
    {
        static $isMainSite;
        static $currentSiteId;

        if (is_null($currentSiteId)) {
            $isMainSite = !$this->view->themeSetting('is_sub_site', false);
            $currentSite = $this->currentSite();
            $currentSiteId = $currentSite->id();
        }

        if ($resource instanceof \Omeka\Api\Representation\SiteRepresentation) {
            return $resource->id() !== $currentSiteId ? $resource : null;
        } elseif ($resource instanceof \Omeka\Api\Representation\SitePageRepresentation) {
            $site = $resource->site();
            return $site->id() !== $currentSiteId ? $site : null;
        } elseif ($resource instanceof \Omeka\Api\Representation\ItemSetRepresentation) {
            // Below.
        } elseif ($resource instanceof \Omeka\Api\Representation\MediaRepresentation) {
            $resource = $resource->item();
        } elseif (!($resource instanceof \Omeka\Api\Representation\ItemRepresentation)) {
            return false;
        }

        $sites = $resource->sites();
        if ((!$isMainSite || !$mainSiteExternal) && isset($sites[$currentSiteId])) {
            return null;
        }
        unset($sites[$currentSiteId]);
        return reset($sites) ?: null;
    }

    /**
     * Manage multiple properties that can contain a heading, a body or another field.
     *
     * @param \Omeka\Api\Representation\AbstractResourceEntityRepresentation $resource
     * @param string $name
     * @return \Omeka\Api\Representation\ValueRepresentation|string|null
     */
    public function elementListValue(\Omeka\Api\Representation\AbstractResourceEntityRepresentation $resource, string $name)
    {
        static $lang;
        static $langValue;
        static $headingTerm;
        static $bodyTerm;
        static $elementBody;
        static $defaultTitle;
        static $listMultifields;
        static $multifields;

        if (is_null($defaultTitle)) {
            $plugins = $this->view->getHelperPluginManager();
            $translate = $plugins->get('translate');
            $siteSetting = $plugins->get('siteSetting');
            $themeSetting = $plugins->get('themeSetting');

            $filterLocale = (bool) $siteSetting('filter_locale_values');
            $siteLang = $plugins->get('lang')();
            $lang = $filterLocale ? $siteLang : null;
            $langValue = $filterLocale ? [$siteLang, ''] : null;

            $elementBody = $themeSetting('list_element_body', 'default');
            $headingTerm = $siteSetting('browse_heading_property_term');
            $bodyTerm = $elementBody === 'none' ? null : $siteSetting('browse_body_property_term');

            $listMultifields = $themeSetting('list_multifields', []);
            $multifields['heading'] = isset($listMultifields['heading']) ? array_filter(explode(' ', str_replace(',', ' ', $listMultifields['heading']))) : [];
            $multifields['heading'] = array_filter(array_unique(array_merge([$headingTerm], $multifields['heading'])));
            if ($elementBody === 'none') {
                $multifields['body'] = [];
            } else {
                $multifields['body'] = isset($listMultifields['body']) ? array_filter(explode(' ', str_replace(',', ' ', $listMultifields['body']))) : [];
                $multifields['body'] = array_filter(array_unique(array_merge([$bodyTerm], $multifields['body'])));
            }
            $multifields['date'] = isset($listMultifields['date']) ? array_filter(explode(' ', str_replace(',', ' ', $listMultifields['date']))) : [];

            $defaultTitle = $translate('[Untitled]');
        }

        switch ($name) {
            case 'heading':
                foreach ($multifields['heading'] as $term) {
                    $value = $resource->value($term, ['lang' => $langValue]);
                    if ($value) {
                        return $value;
                    }
                }
                return $resource->displayTitle($defaultTitle, $lang);

            case 'body':
                foreach ($multifields['body'] as $term) {
                    $value = $resource->value($term, ['lang' => $langValue]);
                    if ($value) {
                        return $value;
                    }
                }
                return $elementBody === 'default'
                    ? $resource->displayDescription(null, $lang)
                    : null;

            default:
                foreach ($multifields[$name] ?? [] as $term) {
                    $value = $resource->value($term, ['lang' => $langValue]);
                    if ($value) {
                        return $value;
                    }
                }
                return null;
        }

        return null;
    }

    /**
     * Transforme un texte en un  tableau avec des entrées pour mediabox.
     */
    public function louvreSearchSeeAlsoEntries(int $count): array
    {
        $siteSetting = $this->view->plugin('siteSetting');
        $themeSetting = $this->view->plugin('themeSetting');
        $seeAlsoLinks = $themeSetting('search_see_also_links');
        if (!$seeAlsoLinks) {
            return [];
        }

        // TODO Sauvegarder dans les themesSettings.
        $seeAlsoLinksReady = $siteSetting('search_see_also_links_ready', []);
        $ser = md5(serialize($seeAlsoLinks));
        if (!isset($seeAlsoLinksReady[$ser])) {
            $site = $this->currentSite();
            $seeAlsoLinksReady[$ser] = $this->listEntries($seeAlsoLinks, $site);
        }

        return $this->listEntryResources($seeAlsoLinksReady[$ser]);
    }

    /**
     * Adaptation.
     *
     * @see \BlockPlus\Site\BlockLayout\Showcase::listEntries()
     *
     * @see \Feed\Controller\FeedController::appendEntries()
     * @todo Better management of Clean url.
     */
    protected function listEntries(array $entries, \Omeka\Api\Representation\SiteRepresentation $site): array
    {
        $result = [];

        $api = $this->view->api();

        $currentSiteId = (int) $site->id();
        $currentSiteSlug = $site->slug();

        $baseEntry = [
            'entry' => null,
            'resource_name' => null,
            'resource' => null,
            'site' => null,
        ];

        foreach ($entries as $entry) {
            $normEntry = $baseEntry;
            $normEntry['entry'] = $entry;
            // Keep empty entries as possible separator.
            if (!$entry) {
                $result[] = $normEntry;
                continue;
            }

            $cleanEntry = trim($entry, '/');
            // Resource?
            if (is_numeric($cleanEntry)) {
                try {
                    $resource = $api->read('resources', ['id' => $cleanEntry])->getContent();
                    $normEntry['resource_name'] = $resource->resourceName();
                    $normEntry['resource'] = $resource->id();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                }
                $result[] = $normEntry;
                continue;
            }

            if (mb_substr($entry, 0, 8) === 'https://' || mb_substr($entry, 0, 7) === 'http://') {
                [$url, $asset, $title, $caption, $body] = array_map('trim', explode('=', $entry, 5));
                $normEntry['data'] = [
                    'url' => $url,
                    'asset' => $asset,
                    'title' => $title,
                    'caption' => $caption,
                    'body' => $body,
                ];
                $result[] = $normEntry;
                continue;
            }

            // Site or page of this site?
            if (!mb_strpos($cleanEntry, '/')) {
                try {
                    $resource = $api->read('sites', ['slug' => $cleanEntry])->getContent();
                    $normEntry['resource_name'] = 'sites';
                    $normEntry['resource'] = $resource->id();
                    $normEntry['site'] = $resource->id();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    try {
                        $resource = $api->read('site_pages', ['site' => $currentSiteId, 'slug' => $cleanEntry])->getContent();
                        $normEntry['resource_name'] = 'site_pages';
                        $normEntry['resource'] = $resource->id();
                        $normEntry['site'] = $currentSiteId;
                    } catch (\Omeka\Api\Exception\NotFoundException $e) {
                        // May be a browse page or a special page.
                    }
                }
                $result[] = $normEntry;
                continue;
            }

            // When the user wants to link a resource on another site: "/s/site/item/1".
            // Manage "item/1", "asset/1", etc. too.
            // TODO Manage the case where the name is not "page" with Clean url.
            $matches = [];
            // Take care of sub-path.
            $r = preg_match('~(?:/?(?:s/)?([^/]+)/)?(pages|page|assets|asset|item_sets|item-set|items|item|media|annotations|annotation)/([^;\?\#]+)~', $entry, $matches);
            if (!$r) {
                $part = mb_strpos($entry, '/') === 0 ? mb_substr($entry, 1) : $entry;
                $matches = [
                    '/s/' . $currentSiteSlug . '/page/' . $part,
                    $currentSiteSlug,
                    'page',
                    $part,
                ];
            }

            $entrySiteId = null;
            if (empty($matches[1]) || $matches[1] === $currentSiteSlug) {
                $entrySiteId = $currentSiteId;
            } else {
                try {
                    $entrySite = $api->read('sites', ['slug' => $matches[1]])->getContent();
                    $entrySiteId = $entrySite->id();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    $result[] = $normEntry;
                    continue;
                }
            }
            if ($matches[2] === 'page' || $matches[2] === 'pages' ) {
                try {
                    $page = $api->read('site_pages', ['site' => $entrySiteId, 'slug' => $matches[3]])->getContent();
                    $normEntry['resource_name'] = 'site_pages';
                    $normEntry['resource'] = $page->id();
                    $normEntry['site'] = $entrySiteId;
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    // Something else.
                }
            } elseif ($matches[2] === 'asset' || $matches[2] === 'assets') {
                try {
                    $asset = $api->read('assets', ['id' => (int) $matches[3]])->getContent();
                    $normEntry['resource_name'] = 'assets';
                    $normEntry['resource'] = $asset->id();
                    $normEntry['site'] = $entrySiteId;
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    // Something else.
                }
            } elseif (is_numeric($matches[3])) {
                try {
                    $resource = $api->read('resources', ['id' => (int) $matches[3]])->getContent();
                    $normEntry['resource_name'] = $resource->resourceName();
                    $normEntry['resource'] = $resource->id();
                    $normEntry['site'] = $entrySiteId;
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    // Something else.
                }
            }
            $result[] = $normEntry;
        }

        return $result;
    }

    protected function listEntryResources(array $entries): array
    {
        $api = $this->view->api();

        foreach ($entries as &$entry) {
            // The site may be private for the user.
            if (!empty($entry['site'])) {
                try {
                    $entry['site'] = $api->read('sites', ['id' => $entry['site']])->getContent();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    $entry['site'] = null;
                }
            }

            if (!empty($entry['resource_name']) && !empty($entry['resource'])) {
                try {
                    $entry['resource'] = $api->read($entry['resource_name'], ['id' => $entry['resource']])->getContent();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    // Something else or private resource.
                    $entry['resource_name'] = null;
                    $entry['resource'] = null;
                }
            }

            if (!empty($entry['data']['asset']) && is_numeric($entry['data']['asset'])) {
                try {
                    $entry['data']['asset'] = $api->read('assets', ['id' => $entry['data']['asset']])->getContent();
                } catch (\Omeka\Api\Exception\NotFoundException $e) {
                    $entry['data']['asset'] = null;
                }
            }
        }
        unset($entry);

        return $entries;
    }
}
